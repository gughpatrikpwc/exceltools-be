package sk.pwc.exceltools.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExcelTable {

    private String name;

    private List<ExcelTableColumn> header;

    private List<List<Object>> data;
}
