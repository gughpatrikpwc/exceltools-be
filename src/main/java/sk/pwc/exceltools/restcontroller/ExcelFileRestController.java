package sk.pwc.exceltools.restcontroller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sk.pwc.exceltools.model.ExcelTable;
import sk.pwc.exceltools.service.ExcelFileDatabaseExportService;

import java.util.Arrays;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/excel")
@AllArgsConstructor
public class ExcelFileRestController {

    private ExcelFileDatabaseExportService excelFileDatabaseExportService;

    @PostMapping("files/export")
    public ResponseEntity<String> exportExcelFile(@RequestParam("files") MultipartFile[] files) {
        Arrays.stream(files).forEach(excelFileDatabaseExportService::exportFile);
        return ResponseEntity.ok("exported");
    }

    @PostMapping("tables/export")
    public ResponseEntity<String> exportTables(@RequestBody List<ExcelTable> tables) {
        tables.forEach(excelFileDatabaseExportService::exportTable);
        return ResponseEntity.ok("exported");
    }
}
