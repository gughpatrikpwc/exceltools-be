package sk.pwc.exceltools.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sk.pwc.exceltools.model.ExcelTable;

import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
@AllArgsConstructor
public class ExcelFileDatabaseExportService {

    private JdbcTemplate jdbcTemplate;

    public void exportFile(MultipartFile file) {
        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            workbook.sheetIterator().forEachRemaining(this::exportSheet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exportTable(ExcelTable table) {
        Map<String, String> headerColumns = new LinkedHashMap<>();
        table.getHeader().forEach(col -> headerColumns.put("\"" + col.getName() + "\"", col.getType()));

        List<String> headerColumnNames = new ArrayList<>(headerColumns.keySet());

        createTableWithColumns(table.getName(), headerColumns);

        table.getData().forEach(row -> insertIntoTable(table.getName(), headerColumnNames, row));
    }

    private void exportSheet(Sheet sheet) {
        Map<String, String> headerColumns = getHeadersFromRow(sheet.getRow(sheet.getFirstRowNum()));
        List<String> headerColumnNames = new ArrayList<>(headerColumns.keySet());

        createTableWithColumns(sheet.getSheetName(), headerColumns);

        for (int rowIdx = sheet.getFirstRowNum() + 1; rowIdx <= sheet.getLastRowNum(); ++rowIdx) {
            insertIntoTable(sheet.getSheetName(), headerColumnNames, getValuesFromRow(sheet.getRow(rowIdx)));
        }
    }

    private void createTableWithColumns(String table, Map<String, String> columns) {
        jdbcTemplate.execute(constructSqlCreateTableQuery(table, columns));
    }

    private void insertIntoTable(String table, List<String> columns, List<Object> values) {
        jdbcTemplate.update(constructSqlInsertQuery(table, columns, values));
    }

    private List<Object> getValuesFromRow(Row row) {
        List<Object> columnNames = new ArrayList<>();

        row.cellIterator().forEachRemaining(cell -> {
            if (cell.getCellType() == CellType.STRING)
                columnNames.add("'" + cell.getStringCellValue() + "'");
            else
                columnNames.add(((XSSFCell) cell).getRawValue());
        });
        return columnNames;
    }

    private Map<String, String> getHeadersFromRow(Row row) {
        Map<String, String> headers = new LinkedHashMap<>();

        // todo: cell type
        row.cellIterator().forEachRemaining(cell ->
                headers.put("\"" + cell.getStringCellValue() + "\"", "VARCHAR(50)"));

        return headers;
    }

    private String constructSqlCreateTableQuery(String table, Map<String, String> columns) {
        String sql = "CREATE TABLE IF NOT EXISTS \"" + table + "\" (" +
                columns.entrySet()
                        .stream()
                        .map(e -> e.getKey() + " " + e.getValue()) // <column_name> <column_type>
                        .collect(joining(", ")) +
                ");";

        log.info(sql);
        return sql;
    }

    private String constructSqlInsertQuery(String table, List<String> columns, List<Object> values) {
        String sql = "INSERT INTO \"" + table + "\" (" +
                String.join(", ", columns) +
                ") VALUES (" +
                values.stream().map(value -> {
                    if (value instanceof String)
                        return "'" + value + "'";
                    else
                        return value.toString();
                }).collect(joining(", ")) +
                ");";

        log.info(sql);
        return sql;
    }
}
